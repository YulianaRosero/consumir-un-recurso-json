
export interface Funcionario {
    nombre:           string;
    cargo:            string;
    dependencia:      string;
    subdependencia:   string;
    oficina:   string;
    tipo_vinculaci_n: string;
}