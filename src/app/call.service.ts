import { Injectable } from '@angular/core';
import {   HttpClient  } from '@angular/common/http';
import { Funcionario } from './funcionario';
@Injectable({
  providedIn: 'root'
})
export class CallService {

  constructor(private http: HttpClient) { }

 called(){
  return this.http.get<Funcionario[]>("https://www.datos.gov.co/resource/7j7g-rasr.json");
 }


}
