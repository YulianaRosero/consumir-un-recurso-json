
import { Component } from '@angular/core';
import { CallService } from './call.service';
import { Funcionario } from './funcionario';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  arreglo:Funcionario[]=[];
  constructor(private callservice:CallService){

    this.consumo();


  }
  consumo(){
     this.callservice.called().subscribe(resp=>this.arreglo= resp.slice(1, 50))

  }
}
